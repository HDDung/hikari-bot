{ WebClient } = require "@slack/client"
moment = require 'moment'
giphy = require('giphy-api')()

# user: {
                #     id,
                #     type,
                #     info: {
                #         id,
                #         name,
                #         real_name,
                #         slack: { ... }
                #         email_address
                #         room
                #     }
                # }

aAttachement = [
  {
    "Text": ""
  },
  {
    "text": "Bao lâu đây anh :thinking_face: :thinking_face:",
    "color": "#3AA3E3",
    "callback_id": "send_meeting",
    "actions": [
      {
        "name": "Duration",
        "text": "Quẩy từ...",
        "type": "select",
        "options": [
          {
            "text": "30 Phút :face_with_rolling_eyes:",
            "value": "30"
          },
          {
            "text": "1 Tiếng :stuck_out_tongue_closed_eyes:",
            "value": "60"
          },
          {
            "text": "2 Tiếng :zany_face:",
            "value": "120"
          },
          {
            "text": "Tới bến :drunk:",
            "value": "1440"
          }
        ]
      }
    ]
  },
  {
    "text": "Gửi chứ, éc ,éc :pig2:",
    "callback_id": "send_meeting",
    "actions": [
      {
        "name": "Cancel",
        "text": "Thôi bỏ",
        "type": "button",
        "value": "cancel"
      },
      {
        "name": "Send request",
        "text": "Gửi nào",
        "type": "button",
        "value": "send_request",
        "confirm": {
          "title": "Éc éc ?",
          "text": "Anh chắc rồi chứ? gửi rồi không hồi được đâu, ư ư",
          "ok_text": "Ừ",
          "dismiss_text": "Từ từ"
        }
      }
    ]
  }
]
                
# check to see if robot is being addressed directly
    # don't want it replying to every message in a channel
    


module.exports = (robot) ->
    web = new WebClient robot.adapter.options.token

    robot.respond /ơi/i, (res) ->
        res.send "Có em đây"
        giphy.random('hello')
        .then (response) ->
            res.send "<#{response.data.image_original_url}>"
        .catch (err) ->
            console.log err

    robot.hear /.*cua gắt/i, (res) ->
        giphy.random('drifts')
        .then (response) ->
            res.send "<#{response.data.image_original_url}>"
        .catch (err) ->
            console.log err

    robot.respond /.* là ai/i, (res) ->
        res.send "Muốn biết em là ai sao ? :wink_old: :wink_old: :wink_old:"
        giphy.random('abs')
        .then (response) ->
            res.send "<#{response.data.image_original_url}>"
        .catch (err) ->
            console.log err

    robot.catchAll (msg) ->
        defaultResponse = "Xin lỗi, anh nói gì em hổng hiểu. :thinking_face: :thinking_face:"
        if robotIsNamed(msg) and defaultResponse isnt ""
            msg.reply defaultResponse
        msg.finish()

    robot.respond /.* với/i, (res) ->
        id = res.message.user.id
        room = res.message.user.room
        message = "Ôi trời, để em book lịch nhé :party_: :party_:"
        oValue = {
            time: getTimeFromMess( res.message )
            aEmail: []
        }

        # filter mentions to just user mentions
        user_mentions = ( mention for mention in res.message.mentions when mention.type is "user" and not mention.info.slack.is_bot )

        # when there are user mentions...
        if user_mentions.length > 0
            sListAttendee = ""
            # process each mention
            for user in user_mentions
                
                oAttendee = {
                    address: user.info.email_address,
                    name: user.info.name
                }
                oValue.aEmail.push oAttendee

                sListAttendee += "<@" +  user.id.toString() + ">, "

            # send the response
            aAttachement[0].text = "Quẩy với ".concat sListAttendee
            aAttachement[2].actions[1].value = JSON.stringify oValue
            attachments = JSON.stringify aAttachement

            web.chat.postMessage( { channel: room, text: message, attachments: attachments } )
            .catch (error) ->
                console.log error
                res.send "Éc, éc, em không gửi được tin nhắn cho anh :sosad: :sosad:"


    robot.respond /read email/i, (res) ->
         robot.http("https://graph.microsoft.com/v1.0/me/mailfolders/inbox/messages?$select=subject,from,receivedDateTime&$top=2&$orderby=receivedDateTime%20DESC")
            .header('Accept', 'application/json')
            .header('X-AnchorMailbox', 'hikari-bot@outlook.com')
            .header('Authorization', Token)
            .get() (err, response, body) ->
                if response.statusCode is 200
                    data = null
                    try
                        data = JSON.parse body
                    catch error
                        res.send "Ran into an error parsing JSON :("
                        return
                    console.log data.value.length
                    res.send "Request come back HTTP 200 :)"

    robotIsNamed = (msg) ->
        r = new RegExp "^(@?#{robot.alias}:?|#{robot.name})", "i"
        matches = msg.message.text.match(r)
        return matches != null && matches.length > 1
    
    getTimeFromMess = (message) ->
        regex = /từ|lúc ([0-9]{2}:[0-9]{2})/
        mess = message.text + ""
        matches = regex.exec(mess)
        console.log matches
        if matches isnt null
            aTime = matches[1].split(":")
            oTime = new Date()
            oTime.setHours(aTime[0])
            oTime.setMinutes(aTime[1])
            oTime.setSeconds(0)
            console.log oTime.getTime()
            return oTime.getTime()
        return null
        

